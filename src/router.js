import Vue from 'vue'
import Router from 'vue-router'
import store from './store/store';

import Home from './views/Home.vue'
import Chat from './views/Chat.vue'

Vue.use(Router)

let router = new Router({
	mode: 'history',
	base: process.env.BASE_URL,
	routes: [
		{
			path: '/chat',
			name: 'chat',
			component: Chat
		},
		{
			path: '/home',
			name: 'home',
			component: Home,
			props: {
				routeForbidden: false
			}
		},
		{
			path: '*',
			redirect: {name: 'home'}
		}
	]
})


router.beforeEach((to, from, next) => {
	store.dispatch('auth/initModule')
		.then(() => {
			if (to.name === 'chat' && !store.getters['auth/isLogged'])
				next({name: 'home', props: {routeForbidden: true}});
			else
				next();
		});
});

export default router;
