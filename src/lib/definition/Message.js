import md5 from 'js-md5'
import moment from 'moment';

export const MessageType = {
	META: 0,
	CONTENT: 1,
};

export class Message {

	get content () {
		return this._content
	}

	set content (value) {
		this._content = value
	}

	get creator () {
		return this._creator
	}

	set creator (value) {
		this._creator = value
	}

	get date () {
		return this._date
	}

	set date (value) {
		this._date = value
	}

	get type () {
		return this._type
	}

	set type (value) {
		this._type = value
	}

	get hash () {
		return this._hash
	}

	set hash (value) {
		this._hash = value
	}

	constructor(content, creator, date, type = MessageType.CONTENT) {
		this._content = content
		this._creator = creator
		this._date = date
		this._type = type

		this._hash = md5(JSON.stringify(this));
	}

	static build(message) {
		return new Message(message._content, message._creator, moment(message._date), message._type)
	}
}
