
const state = {
	user: {
		username: '',
		logged: false,
	},
	ready: false,
};

const mutations = {
	INIT: (state , {user}) => {
		state.user = user;
		state.ready = true;
	},
	LOG_USER: (state, { name }) => {
		state.user.username = name;
		state.user.logged = true;

		window.sessionStorage.setItem('user', JSON.stringify(state.user));
	},
	UNLOG_USER: (state) => {
		state.user.username = '';
		state.user.logged = false;

		window.sessionStorage.removeItem('user');
	}
};

const actions = {
	initModule(store) {
		return new Promise((resolve) => {
			if (!store.state.ready) {
				let user = window.sessionStorage.getItem('user');
				if (user !== null && user !== undefined){
					store.commit('INIT', {user: JSON.parse(user)});
				} else {
					store.commit('INIT', {user: {username: '', logged: false}});
				}
			}
			resolve();
		});
	},
	connectUser(store, { username }) {
		return new Promise((resolve) => {
			store.commit('LOG_USER', {name: username});
			resolve();
		})
	},
	disconnectUser(store) {
		return new Promise((resolve) => {
			store.commit('UNLOG_USER');
			resolve();
		})
	}
};

const getters = {
	getUser: state => state.user,
	isLogged: state => state.user.logged,
};

export default {
	namespaced: true,
	state,
	mutations,
	actions,
	getters,
};
